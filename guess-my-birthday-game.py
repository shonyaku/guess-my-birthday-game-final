from random import randint
guess_counter = 1
name = input("Hi! What is your name? ")

for guess_counter in range(1, 6):

    year_guess = randint(1924, 2004)
    month_guess = randint(1, 12)
    day_guess = randint(1, 31)
    
    print("Guess", guess_counter, ":", name, "were you born on", month_guess, "/", day_guess, "/", year_guess, "?")
    player_answer = input("yes or no? ")

    if player_answer == "yes":
        print("I knew it!")
        exit()
    else:
        if guess_counter <= 4:
            print("Drat! Lemme try again!")
            guess_counter = guess_counter + 1
        else:
            print("I have other things to do. Good bye.")
            exit()